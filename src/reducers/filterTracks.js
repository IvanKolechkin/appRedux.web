const initialState = '';

export default function filterTracks (state = initialState, action) {
    //Ловлю action и решаю что делать
    if (action.type === 'FIND_TRACK'){
        return action.payload;
    }
    return state;
}
