const initialState = [
        'Home',
        'Work'
    ];

export default function playlist(state = initialState, action) {
    //Ловлю action и решаю что делать
    if (action.type === 'ADD_PLAYLIST'){
        //Не можем менять именно state
        //Можем вернуть лишь новую копию state
        // Это спред массив + новый элемент
        return state;
    }
    return state;
}
