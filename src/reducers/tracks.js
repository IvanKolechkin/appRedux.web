const initialState = [];

export default function tracks (state = initialState, action) {
    //Ловлю action и решаю что делать
    if (action.type === 'ADD_TRACK'){
        //Не можем менять именно state
        //Можем вернуть лишь новую копию state
        // Это спред массив + новый элемент
        return [
            ...state,
            action.payload
        ];
    } else if (action.type === 'FETCH_TRACKS_SUCCESS'){
        return action.payload;
    }
    return state;
}
