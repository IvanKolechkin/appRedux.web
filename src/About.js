import React, { Component } from 'react';
import Menu from './Menu';

class About extends Component {
    render() {
        return (
            <div>
                <Menu/>
                <h1>About</h1>
            </div>
        );
    }
}

export default About;
