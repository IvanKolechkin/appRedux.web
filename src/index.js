import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux'
import { createStore, applyMiddleware } from 'redux';
import { composeWithDevTools  } from 'redux-devtools-extension';
import thunk from 'redux-thunk';

//Работает только на react-router 3
import { HashRouter, Route } from 'react-router-dom';


import './index.css';
import App from './App';
import About from './About';
import reducer from './reducers';
//import registerServiceWorker from './registerServiceWorker';

const initialState = {
    tracks: [
        'Smells',
        'test 2'
    ],
    playlist: [
        'Home playlist',
        'Work playlist'
    ]
};



//СОздаю store
const store = createStore(reducer, composeWithDevTools(applyMiddleware(thunk)));


//Уже не рабочий вариант
/*<Provider store={store}>
    <Router history={hashHistory}>
        <Route path="/" component={App}/>
    </Router>
</Provider>*/

ReactDOM.render(
    <Provider store={store}>
        <HashRouter>
            <div>
                <Route exact path="/" component={App} />
                <Route path="/about" component={About} />
            </div>
        </HashRouter>
    </Provider>,
    document.getElementById('root'));
//registerServiceWorker();
/*
//Импортирую createstore
import { createStore } from 'redux';

function playlist(state = [], action) {
    //Ловлю action и решаю что делать
    if (action.type === 'ADD_TRACK'){
        //Не можем менять именно state
        //Можем вернуть лишь новую копию state
        // Это спред массив + новый элемент
        return [
            ...state,
            action.payload
        ];
    }
    return state;
}

//СОздаю store
const store = createStore(playlist);

const addTrackBtn = document.querySelectorAll('.addTrack')[0];
const addTrackInput = document.querySelectorAll('.trackInput')[0];
const list =document.querySelectorAll('.list')[0];

//Выводим store в консоль (объект состояний всего приложения)
console.log(store.getState());

//Подписываюсь на изменения store. Указывается callback функция
store.subscribe(() => {
    //Очищаю list
    list.innerHTML = '';

    //Вывожу данные из store в list
    store.getState().forEach(track => {
        const li = document.createElement("li");
        li.textContent = track;
        list.appendChild(li);
    });
})


addTrackBtn.addEventListener('click',() =>{
    const trackName = addTrackInput.value;
    //Изменить store можно только через dispatch (событие), его метод action (объект)
    store.dispatch({ type: 'ADD_TRACK', payload: trackName});
    //Очищаю input
    addTrackInput.value = '';

});
*/
