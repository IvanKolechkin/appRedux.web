var mockApiData = [
    {
        id: 1,
        name: 'Name track 1'
    },
    {
        id: 2,
        name: 'Name track 2'
    },
    {
        id: 3,
        name: 'Name track 3'
    },
    {
        id: 4,
        name: 'Name track 4'
    },
    {
        id: 5,
        name: 'Name track 5'
    }
];

export const getTracks = () => dispatch =>{
    setTimeout(()=>{
        console.log('I got tracks');
        dispatch({ type: 'FETCH_TRACKS_SUCCESS', payload: mockApiData});
    }, 2000);
}
